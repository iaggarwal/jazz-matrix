import { Component, OnInit } from "@angular/core";
import { MatrixService } from "./services/matrix.service";
import { Observable } from "rxjs";
import { Position } from "./interfaces/position.interface";
import { TraversalResponse } from "./interfaces/traversal-response.interface";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: "jn-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  constructor(
    private readonly matrixService: MatrixService,
    private readonly fb: FormBuilder
  ) {}

  public title = "jazz-networks";
  public matrix: Observable<Array<Array<number>>>;
  public size: number;
  public traversalResponse: TraversalResponse;
  public backGroundColors: object = {
    activeCell: "red",
    activeCellHover: "orange",
    inActiveCell: "transparent"
  };

  form: FormGroup;

  ngOnInit(): void {
    this.setChangedMatric(MatrixService.MATRIX_SIZE);
    this.form = this.fb.group({
      matrixSize: [10, Validators.required],
      activeCellColor: ["red", Validators.required],
      inactiveCellColor: ["transparent", Validators.required],
      activeCellHoverColor: ["orange", Validators.required]
    });

    this.form.get("matrixSize").valueChanges.subscribe(value => {
      this.setChangedMatric(value);
    });

    this.form.get("activeCellColor").valueChanges.subscribe(value => {
      this.backGroundColors = {
        ...this.backGroundColors,
        activeCell: value
      };
    });
    this.form.get("inactiveCellColor").valueChanges.subscribe(value => {
      this.backGroundColors = {
        ...this.backGroundColors,
        inactiveCellColor: value
      };
    });
    this.form.get("activeCellHoverColor").valueChanges.subscribe(value => {
      this.backGroundColors = {
        ...this.backGroundColors,
        activeCellHover: value
      };
    });
  }

  setChangedMatric(size: number): void {
    this.size = size;
    this.matrix = this.matrixService.getMatrix(size);
  }

  traverse(position: Position) {
    const response = this.matrixService.findCount(position);
    this.traversalResponse = { ...this.traversalResponse, ...response };
  }
}
