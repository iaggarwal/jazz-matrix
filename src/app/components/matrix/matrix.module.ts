import { NgModule } from "@angular/core";
import { MatrixComponent } from "./matrix.component";
import { CommonModule } from "@angular/common";

@NgModule({
  imports: [CommonModule],
  declarations: [MatrixComponent],
  exports: [MatrixComponent]
})
export class MatrixModule {}
