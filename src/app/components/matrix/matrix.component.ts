import {
  Component,
  ChangeDetectionStrategy,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges
} from "@angular/core";
import { Position } from "src/app/interfaces/position.interface";
import { TraversalResponse } from "../../interfaces/traversal-response.interface";

@Component({
  selector: "jn-matrix",
  templateUrl: "./matrix.component.html",
  styleUrls: ["./matrix.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MatrixComponent implements OnInit, OnChanges {
  @Input() matrix: Array<Array<number>>;
  @Input() size: number;
  @Input() traversalResponse: TraversalResponse = {} as TraversalResponse;
  @Input() backgroundColors: object;
  @Output()
  traverse = new EventEmitter<Position>();

  columnWidth: number;
  clickedKey: string;
  cellStyleObject: object;
  transformedMatrix: Array<Array<object>>;
  cellDimentions = {};
  count = 0;
  onMouseEnterKey = "";
  fontSize = 16;

  ngOnInit() {
    this.setDimentions();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["size"] && changes["size"].currentValue) {
      this.setDimentions();
      this.traversalResponse = {} as TraversalResponse;
    }

    if (changes["matrix"] && changes["matrix"].currentValue) {
      this.traversalResponse = {} as TraversalResponse;
      this.count = undefined;
    }

    if (
      changes["traversalResponse"] &&
      changes["traversalResponse"].currentValue
    ) {
      if (this.onMouseEnterKey === this.clickedKey || this.count === 0) {
        this.count = this.traversalResponse.count;
      }
    }
  }

  setDimentions() {
    this.columnWidth = Math.floor(60 / this.size);
    this.cellDimentions = {
      "width.%": this.columnWidth,
      "paddingTop.%": this.columnWidth
    };
  }

  cellClicked(yIndex: number, xIndex: number) {
    const key = `${yIndex}-${xIndex}`;
    if (this.matrix[yIndex][xIndex] === 1) {
      this.clickedKey = key;
      if (
        !this.traversalResponse ||
        !this.traversalResponse.matchedNodes[key]
      ) {
        this.traversalResponse = undefined;
        this.traverse.emit({
          yIndex,
          xIndex
        });
      } else {
        this.count = this.traversalResponse.count;
      }
    }
  }

  showCount(yIndex: number, xIndex: number): boolean {
    return this.clickedKey === `${yIndex}-${xIndex}`;
  }

  onMouseEnter(yIndex: number, xIndex: number) {
    const key = `${yIndex}-${xIndex}`;
    if (
      this.matrix[yIndex][xIndex] === 1 &&
      (!this.traversalResponse ||
        !this.traversalResponse.matchedNodes ||
        !this.traversalResponse.matchedNodes[key])
    ) {
      this.traversalResponse = undefined;
      this.traverse.emit({
        yIndex,
        xIndex
      });
    }
  }

  onMouseLeave(yIndex: number, xIndex: number) {
    this.traversalResponse = {} as TraversalResponse;
  }

  getBackgroundColor(yIndex: number, xIndex: number): string {
    const key = `${yIndex}-${xIndex}`;
    if (this.matrix[yIndex][xIndex] === 0) {
      return this.backgroundColors["inActiveCell"];
    } else {
      if (
        this.traversalResponse &&
        this.traversalResponse.matchedNodes &&
        !!this.traversalResponse.matchedNodes[key]
      ) {
        return this.backgroundColors["activeCellHover"];
      } else {
        return this.backgroundColors["activeCell"];
      }
    }
  }
}
