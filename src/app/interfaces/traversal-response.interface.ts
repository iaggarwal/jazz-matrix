import { Position } from "src/app/interfaces/position.interface";

export interface TraversalResponse {
  count: number;
  matchedNodes: { [key: string]: Position };
}
