export interface Position {
    xIndex: number;
    yIndex: number;
}
