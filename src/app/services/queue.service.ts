import { LinkedList } from './linked-list.service';

export class Queue<T> {
  private list: LinkedList<T>;

  constructor() {
    this.list = new LinkedList<T>();
  }

  get length() {
    return this.list.length;
  }

  push(data: T) {
    this.list.add(data);
  }

  pop(): T {
    return this.list.removeNodeAtPosition(0);
  }

  remove(data: T) {
    this.list.remove(data);
  }

  isEmpty(): boolean {
    return !this.list || !this.list.length || this.list.length < 1;
  }

  forEach(callBackFn: (value: T, index: number) => void) {
    this.list.forEach(callBackFn);
  }

  map<U>(callBackFn: (value: T, index: number) => U): Queue<U> {
    const returnedQueue: Queue<U> = new Queue<U>();
    this.list.map(callBackFn).forEach((val: U) => {
      returnedQueue.push(val);
    });
    return returnedQueue;
  }

  filter(callBackFn: (value: T, index: number) => boolean): Queue<T> {
    const returnedQueue: Queue<T> = new Queue<T>();
    this.list.filter(callBackFn).forEach((val: T) => {
      returnedQueue.push(val);
    });
    return returnedQueue;
  }
}
