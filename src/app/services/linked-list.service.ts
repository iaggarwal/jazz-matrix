class ListNode<T> {
  private nextNode?: ListNode<T>;
  private previousNode?: ListNode<T>;

  constructor(private data: T) {}

  getData(): T {
    return this.data;
  }

  getNextNode(): ListNode<T> {
    return this.nextNode;
  }

  getPreviousNode(): ListNode<T> {
    return this.previousNode;
  }

  setNextNode(node: ListNode<T>) {
    this.nextNode = node;
  }

  setPreviousNode(node: ListNode<T>) {
    this.previousNode = node;
  }
}

export class LinkedList<T> {
  private head: ListNode<T>;
  private tail: ListNode<T>;
  length = 0;

  add(value: T): ListNode<T> {
    const node = new ListNode<T>(value);
    if (this.head) {
      let currentNode = this.head;
      while (currentNode.getNextNode()) {
        currentNode = currentNode.getNextNode();
      }
      currentNode.setNextNode(node);
      node.setPreviousNode(currentNode);
      this.tail = node;
    } else {
      this.head = node;
      this.tail = node;
    }

    this.length++;
    return node;
  }

  remove(value: T) {
    if (!this.head) {
      let currentNode = this.head;
      while (currentNode) {
        if (currentNode.getData() === value) {
          if (currentNode.getPreviousNode()) {
            currentNode
              .getPreviousNode()
              .setNextNode(currentNode.getNextNode());
          }
          if (currentNode.getNextNode()) {
            currentNode
              .getNextNode()
              .setPreviousNode(currentNode.getPreviousNode());
          }

          currentNode = undefined;
          this.length--;
        }
      }
    }
  }

  removeNodeAtPosition(position: number): T {
    let currentNode: ListNode<T> = this.head;
    let data: T;

    if (position < 0 || position >= this.length) {
      throw new Error("Invalid position");
    }

    if (position === 0) {
      data = this.head.getData();
      this.head = this.head.getNextNode();
      if (this.head) {
        this.head.setPreviousNode(undefined);
      }
      this.length--;
    } else if (position === this.length - 1) {
      data = this.tail.getData();
      this.tail = this.tail.getPreviousNode();
      this.tail.setNextNode(undefined);
      this.length--;
    } else {
      let counter = 0;
      while (counter < position) {
        counter++;
        currentNode = currentNode.getNextNode();
      }
      data = currentNode.getData();
      if (currentNode.getPreviousNode()) {
        currentNode.getPreviousNode().setNextNode(currentNode.getNextNode());
      }

      if (currentNode.getNextNode()) {
        currentNode
          .getNextNode()
          .setPreviousNode(currentNode.getPreviousNode());
      }

      this.length--;
    }
    return data;
  }

  forEach(callBackFn: (value: T, index: number) => void) {
    let currentNode = this.head;
    let counter = 0;
    while (currentNode.getNextNode()) {
      callBackFn(currentNode.getData(), counter);
      currentNode = currentNode.getNextNode();
      counter++;
    }
  }

  map<U>(callBackFn: (value: T, index: number) => U): LinkedList<U> {
    const returnedList = new LinkedList<U>();
    let counter = 0;
    let currentNode = this.head;
    while (currentNode.getNextNode()) {
      const modifiedVal: U = callBackFn(currentNode.getData(), counter);
      returnedList.add(modifiedVal);
      currentNode = currentNode.getNextNode();
      counter++;
    }
    return returnedList;
  }

  filter(callBackFn: (value: T, index: number) => boolean): LinkedList<T> {
    const returnedList = new LinkedList<T>();
    let counter = 0;
    let currentNode = this.head;
    while (currentNode.getNextNode()) {
      if (callBackFn(currentNode.getData(), counter)) {
        returnedList.add(currentNode.getData());
      }
      currentNode = currentNode.getNextNode();
      counter++;
    }
    return returnedList;
  }
}
