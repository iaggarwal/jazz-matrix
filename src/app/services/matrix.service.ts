import { Queue } from "./queue.service";
import { Position } from "../interfaces/position.interface";
import { Observable, of } from "rxjs";
import { TraversalResponse } from "../interfaces/traversal-response.interface";

export class MatrixService {
  static readonly MATRIX_SIZE = 10;
  matrix = [
    [0, 0, 0, 0, 1],
    [1, 1, 0, 0, 0],
    [1, 1, 0, 1, 1],
    [0, 0, 0, 0, 0],
    [1, 1, 1, 0, 0]
  ];

  visited: { [key: string]: Position } = {};

  findCount(startPosition: Position): TraversalResponse {
    this.visited = {};
    const queue: Queue<Position> = new Queue<Position>();
    queue.push(startPosition);
    this.addToVisited(startPosition);
    const count = this.traverse(0, queue);
    return {
      count,
      matchedNodes: this.visited
    };
  }

  traverse(counter: number, queue: Queue<Position>): number {
    if (queue.length === 0) {
      return counter;
    }
    const currentPosition = queue.pop();
    counter++;
    const adjecentPositions =
      this.getValidAdjecentPositions(currentPosition) || [];
    adjecentPositions.forEach(pos => {
      queue.push(pos);
      this.addToVisited(pos);
    });

    return this.traverse(counter, queue);
  }

  getValidAdjecentPositions(position: Position): Array<Position> {
    return [
      {
        xIndex: position.xIndex - 1,
        yIndex: position.yIndex
      },
      {
        xIndex: position.xIndex + 1,
        yIndex: position.yIndex
      },
      {
        xIndex: position.xIndex,
        yIndex: position.yIndex - 1
      },
      {
        xIndex: position.xIndex,
        yIndex: position.yIndex + 1
      }
    ].filter((pos: Position) => {
      return (
        this.isIndexInBounds(pos.xIndex) &&
        this.isIndexInBounds(pos.yIndex) &&
        !this.isVisited(pos) &&
        this.matrix[pos.yIndex][pos.xIndex] === 1
      );
    });
  }

  addToVisited(pos: Position): void {
    this.visited[`${pos.yIndex}-${pos.xIndex}`] = pos;
  }

  isVisited(pos: Position): boolean {
    return !!this.visited[`${pos.yIndex}-${pos.xIndex}`];
  }

  isIndexInBounds(index: number) {
    return index > -1 && index < this.matrix.length;
  }

  getMatrix(size: number): Observable<Array<Array<number>>> {
    const matrix: Array<Array<number>> = [];
    for (let yIndex = 0; yIndex < size; yIndex++) {
      matrix[yIndex] = [];
      for (let xIndex = 0; xIndex < size; xIndex++) {
        const randomNumber = Math.random() * 10;
        if (randomNumber < 5) {
          matrix[yIndex].push(0);
        } else {
          matrix[yIndex].push(1);
        }
      }
    }
    this.matrix = matrix;
    return of(this.matrix);
  }
}
