import { MatrixService } from './matrix.service';
describe('MatrixService', () => {
  describe('findCount', () => {
    let ms: MatrixService;
    beforeEach(() => {
      ms = new MatrixService();
      ms.matrix = [
        [0, 0, 0, 0, 1],
        [1, 1, 0, 0, 0],
        [1, 1, 0, 1, 1],
        [0, 0, 0, 0, 0],
        [1, 1, 1, 0, 0]
      ];
    });

    it('should call traverse', () => {
      spyOn(ms, 'traverse');
      ms.findCount({
        xIndex: 0,
        yIndex: 1
      });

      expect(ms.traverse).toHaveBeenCalled();
    });

    it('should return count as 4', () => {
      const response = ms.findCount({
        xIndex: 0,
        yIndex: 1
      });

      expect(response.count).toEqual(4);
    });

    it('should return count as 1', () => {
      const response = ms.findCount({
        xIndex: 4,
        yIndex: 0
      });

      expect(response.count).toEqual(1);
    });

    it('should return count as 3', () => {
      const response = ms.findCount({
        xIndex: 0,
        yIndex: 4
      });

      expect(response.count).toEqual(3);
    });
  });
});
